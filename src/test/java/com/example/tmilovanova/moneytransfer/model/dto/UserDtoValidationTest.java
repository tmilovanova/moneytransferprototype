package com.example.tmilovanova.moneytransfer.model.dto;

import com.example.tmilovanova.moneytransfer.utils.AbstractDtoValidationTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserDtoValidationTest extends AbstractDtoValidationTest<UserDto> {

    @Test
    public void testValidDtoHasNoValidationErrors() {
        UserDto validDto = createValidDto();
        testValidation(validDto, Collections.emptyList());
    }

    @Test
    public void testUserNameShouldNotBeBlank() {
        final String expectedErrorMessage = String.format("name#{%s}", NOT_BLANK_VALIDATOR_MESSAGE_DESCRIPTOR);
        UserDto dto = createValidDto();
        testNotBlank(dto, dto::setName, Collections.singletonList(expectedErrorMessage));
    }

    @Test
    public void testUserPasswordShouldNotBeBlank() {
        final String expectedErrorMessage = String.format("password#{%s}", NOT_BLANK_VALIDATOR_MESSAGE_DESCRIPTOR);
        UserDto dto = createValidDto();
        testNotBlank(dto, dto::setPassword, Collections.singletonList(expectedErrorMessage));
    }

    @Test
    public void testUserEmailShouldNotBeBlank() {
        UserDto dto = createValidDto();
        final String[] caseWithOneViolation = new String[] { null, "" };
        for (String testCase : caseWithOneViolation) {
            dto.setEmail(testCase);
            testValidation(dto, Collections.singletonList(String.format("email#{%s}", NOT_BLANK_VALIDATOR_MESSAGE_DESCRIPTOR)));
        }
        dto.setEmail("   ");
        testValidation(dto, Arrays.asList(
                String.format("email#{%s}", EMAIL_VALIDATOR_MESSAGE_DESCRIPTOR),
                String.format("email#{%s}", NOT_BLANK_VALIDATOR_MESSAGE_DESCRIPTOR)));
    }

    @Test
    public void testUserEmailShouldBeValidEmail() {
        final List<String> expectedErrorMessages =
                Collections.singletonList(String.format("email#{%s}", EMAIL_VALIDATOR_MESSAGE_DESCRIPTOR));
        UserDto dto = createValidDto();

        dto.setEmail("invalid");
        testValidation(dto, expectedErrorMessages);

        dto.setEmail("invalid@");
        testValidation(dto, expectedErrorMessages);

        dto.setEmail("invalid@domain@");
        testValidation(dto, expectedErrorMessages);
    }

    @Override
    protected UserDto createValidDto() {
        UserDto userDto = new UserDto();
        userDto.setEmail("fake@fake.domain.com");
        userDto.setName("John Doe");
        userDto.setPassword("qwerty");
        return userDto;
    }
}