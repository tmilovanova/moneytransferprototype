package com.example.tmilovanova.moneytransfer.resource;

import com.example.tmilovanova.moneytransfer.exceptions.RestError;
import com.example.tmilovanova.moneytransfer.model.dto.UserDto;
import com.example.tmilovanova.moneytransfer.model.response.AccountResponse;
import com.example.tmilovanova.moneytransfer.model.response.UserResponse;
import lombok.SneakyThrows;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;

import static com.example.tmilovanova.moneytransfer.utils.HttpUtils.UNPROCESSABLE_ENTITY_CODE;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserResourceIT extends AbstractResourceIT {

    @Test
    public void testCreateUserSuccessfullyCreatesUserInTheDatabase() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        UserResponse userResponse = response.readEntity(UserResponse.class);
        assertValidResponse(userResponse, fakeUserRequest);

        assertThat(performGetByIdWithObject(userResponse.getId(), UserResponse.class)).isEqualTo(userResponse);
    }

    @Test
    public void testCreateUserWithIncorrectRequestFails() {
        Response response = performPost(createInvalidUserRequest());

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(UNPROCESSABLE_ENTITY_CODE);
    }

    @Test
    public void testCreateUserWithDuplicateEmailFails() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        response = performPost(fakeUserRequest);
        assertThat(response.getStatus()).isEqualTo(Response.Status.CONFLICT.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.EMAIL_ALREADY_REGISTERED.getMessage());
    }

    @Test
    public void testUpdateUserSuccessfullyUpdatesTheExistingUserInTheDatabase() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        final String id = response.readEntity(UserResponse.class).getId();
        final String newName = "Jane Doe";
        UserDto updateUserRequest = createFakeUserRequest();
        updateUserRequest.setName(newName);

        response = performPut(id, updateUserRequest);
        UserResponse userResponse = response.readEntity(UserResponse.class);
        assertThat(userResponse.getId()).isEqualTo(id);
        assertValidResponse(userResponse, updateUserRequest);

        assertThat(performGetByIdWithObject(userResponse.getId(), UserResponse.class)).isEqualTo(userResponse);
    }

    @Test
    public void testUpdateUserFailsIfUpdatedEmailValueAlreadyRegistered() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response firstUserResponse = performPost(fakeUserRequest);
        UserDto anotherFakeUserRequest = createFakeUserRequest();
        Response secondUserResponse = performPost(anotherFakeUserRequest);

        assertThat(firstUserResponse).isNotNull();
        assertThat(firstUserResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertThat(secondUserResponse).isNotNull();
        assertThat(secondUserResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        UserResponse secondUser = secondUserResponse.readEntity(UserResponse.class);
        final String id = secondUser.getId();
        final String newEmail = firstUserResponse.readEntity(UserResponse.class).getEmail();
        UserDto updateUserRequest = createFakeUserRequest();
        updateUserRequest.setEmail(newEmail);

        Response response = performPut(id, updateUserRequest);
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CONFLICT.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.EMAIL_ALREADY_REGISTERED.getMessage());

        assertThat(performGetByIdWithObject(id, UserResponse.class)).isEqualTo(secondUser);
    }


    @Test
    public void testUpdateUserWithIncorrectRequestFails() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        UserResponse initialUser = response.readEntity(UserResponse.class);

        fakeUserRequest.setEmail("");
        response = performPut(initialUser.getId(), fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(UNPROCESSABLE_ENTITY_CODE);

        assertThat(performGetByIdWithObject(initialUser.getId(), UserResponse.class)).isEqualTo(initialUser);
    }

    @Test
    public void testUpdateNonExistingUserFails() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPut(NON_EXISTING_USER_ID, fakeUserRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.USER_NOT_FOUND.getMessage());
    }

    @Test
    public void testGetNonExistingUserFails() {
        Response response = performGetById(NON_EXISTING_USER_ID);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.USER_NOT_FOUND.getMessage());
    }

    @Test
    public void testGetNonExistingUserByEmailFails() {
        Response response = performGetByEmail(createFakeUserRequest().getEmail());

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.USER_NOT_FOUND.getMessage());
    }

    @Test
    public void testGetExistingUserByEmailSuccessfullyRetrievesUser() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);
        UserResponse initialUser = response.readEntity(UserResponse.class);

        Response responseByEmail = performGetByEmail(fakeUserRequest.getEmail());

        assertThat(responseByEmail).isNotNull();
        assertThat(responseByEmail.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(responseByEmail.readEntity(UserResponse.class)).isEqualTo(initialUser);
    }

    @Test
    public void testDeletingNonExistingUserIsNoop() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);
        UserResponse initialUser = response.readEntity(UserResponse.class);

        Response deleteResponse = performDelete(NON_EXISTING_USER_ID);
        assertThat(deleteResponse).isNotNull();
        assertThat(deleteResponse.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());
        assertThat(deleteResponse.readEntity(String.class)).isEqualTo("");

        UserResponse afterDeletion = performGetById(initialUser.getId()).readEntity(UserResponse.class);
        assertThat(afterDeletion).isEqualTo(initialUser);
    }

    @Test
    public void testDeletingExistingUserIsSuccessful() {
        UserDto fakeUserRequest = createFakeUserRequest();
        Response response = performPost(fakeUserRequest);
        UserResponse initialUser = response.readEntity(UserResponse.class);

        AccountResponse accountResponse = createAccount(initialUser.getId(), BigDecimal.ZERO);

        Response deleteResponse = performDelete(initialUser.getId());
        assertThat(deleteResponse).isNotNull();
        assertThat(deleteResponse.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());
        assertThat(deleteResponse.readEntity(String.class)).isEqualTo("");

        Response afterDeletion = performGetById(initialUser.getId());
        assertThat(afterDeletion).isNotNull();
        assertThat(afterDeletion.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(afterDeletion.readEntity(String.class)).isEqualTo(RestError.USER_NOT_FOUND.getMessage());

        assertThat(performGetById(ACCOUNTS_PATH,
                accountResponse.getId()).getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());

        // TODO add Resource tests that will allow to mock Repositories and test data integrity in case of
        // an interrupted transaction.
    }

    private void assertValidResponse(final UserResponse userResponse, final UserDto fakeUserRequest) {
        assertThat(userResponse.getId()).isNotEmpty();
        assertThat(userResponse.getName()).isEqualTo(fakeUserRequest.getName());
        assertThat(userResponse.getEmail()).isEqualTo(fakeUserRequest.getEmail());
        assertThat(userResponse.getPassword()).isEqualTo(fakeUserRequest.getPassword());
    }

    private Response performGetByEmail(String userId) {
        return client.target(toUri(userEmailEndpointPath(userId))).request().get();
    }

    private UserDto createInvalidUserRequest() {
        return new UserDto();
    }

    @SneakyThrows(UnsupportedEncodingException.class)
    private String userEmailEndpointPath(String email) {
        return getPath() + "?email=" + URLEncoder.encode(email, "UTF-8");
    }

    @Override
    protected String getPath() {
        return USERS_PATH;
    }
}