package com.example.tmilovanova.moneytransfer.resource;

import com.example.tmilovanova.moneytransfer.exceptions.RestError;
import com.example.tmilovanova.moneytransfer.model.dto.AccountDto;
import com.example.tmilovanova.moneytransfer.model.response.AccountResponse;
import com.example.tmilovanova.moneytransfer.model.response.UserResponse;
import lombok.SneakyThrows;
import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import static com.example.tmilovanova.moneytransfer.utils.HttpUtils.UNPROCESSABLE_ENTITY_CODE;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountResourceIT extends AbstractResourceIT {

    @Test
    public void testCreateAccountSuccessfullyCreatesAccountInTheDatabase() {
        UserResponse user = createUser();

        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        AccountResponse accountResponse = response.readEntity(AccountResponse.class);
        assertValidResponse(accountResponse, fakeAccountRequest);

        assertThat(performGetByIdWithObject(accountResponse.getId(), AccountResponse.class)).isEqualTo(accountResponse);
    }

    @Test
    public void testCreateAccountWithNonExistingUserFails() {
        AccountDto fakeAccountRequest = createFakeAccountRequest(NON_EXISTING_USER_ID, BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.USER_NOT_FOUND.getMessage());
    }

    @Test
    public void testCreateAccountWithIncorrectRequestFails() {
        Response response = performPost(createInvalidAccountRequest());

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(UNPROCESSABLE_ENTITY_CODE);
    }

    @Test
    public void testUpdateAccountSuccessfullyUpdatesAccountInTheDatabase() {
        UserResponse user = createUser();

        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        AccountResponse initialAccount = response.readEntity(AccountResponse.class);

        final BigDecimal newAmount = BigDecimal.ONE;
        fakeAccountRequest.setBalance(newAmount);

        response = performPut(initialAccount.getId(), fakeAccountRequest);
        AccountResponse accountResponse = response.readEntity(AccountResponse.class);
        assertThat(accountResponse.getId()).isEqualTo(initialAccount.getId());
        assertValidResponse(accountResponse, fakeAccountRequest);

        assertThat(performGetByIdWithObject(accountResponse.getId(), AccountResponse.class)).isEqualTo(accountResponse);
    }

    @Test
    public void testUpdatingNonExistingAccountFails() {
        UserResponse user = createUser();
        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPut(NON_EXISTING_ACCOUNT_ID, fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.ACCOUNT_NOT_FOUND.getMessage());
    }

    @Test
    public void testUpdateUserWithIncorrectRequestFails() {
        UserResponse user = createUser();

        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        AccountResponse initialAccount = response.readEntity(AccountResponse.class);

        fakeAccountRequest.setBalance(null);
        response = performPut(initialAccount.getId(), fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(UNPROCESSABLE_ENTITY_CODE);

        assertThat(performGetByIdWithObject(initialAccount.getId(), AccountResponse.class)).isEqualTo(initialAccount);
    }

    @Test
    public void testUpdatingUserIdInAccountIsNotSupported() {
        UserResponse user = createUser();

        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        AccountResponse initialAccount = response.readEntity(AccountResponse.class);

        fakeAccountRequest.setUserId("1");
        response = performPut(initialAccount.getId(), fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(RestError.CANNOT_CHANGE_ACCOUNT_OWNER.getHttpCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.CANNOT_CHANGE_ACCOUNT_OWNER.getMessage());

        assertThat(performGetByIdWithObject(initialAccount.getId(), AccountResponse.class)).isEqualTo(initialAccount);
    }

    @Test
    public void testGetNonExistingAccountFails() {
        Response response = performGetById(NON_EXISTING_ACCOUNT_ID);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.ACCOUNT_NOT_FOUND.getMessage());
    }

    @Test
    public void testGetAccountsByUserIdSuccessfullyRetrievesExistingAccounts() {
        UserResponse user = createUser();

        Response response = performPost(createFakeAccountRequest(user.getId(), BigDecimal.ZERO));
        AccountResponse firstFakeAccount = response.readEntity(AccountResponse.class);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        response = performPost(createFakeAccountRequest(user.getId(), BigDecimal.ONE));
        AccountResponse secondFakeAccount = response.readEntity(AccountResponse.class);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        response = performGetByUserId(user.getId());
        List<AccountResponse> responses = response.readEntity(new GenericType<List<AccountResponse>>() {
        });
        assertThat(responses).hasSize(2);
        assertThat(responses).containsExactly(firstFakeAccount, secondFakeAccount);
    }

    @Test
    public void testGetAccountByNonExistingUserIdReturnsEmptyList() {
        Response response = performGetByUserId(NON_EXISTING_USER_ID);
        List<AccountResponse> responses = response.readEntity(new GenericType<List<AccountResponse>>() {
        });
        assertThat(responses).isEmpty();
    }

    @Test
    public void testDeleteExistingAccountIsSuccessful() {
        UserResponse user = createUser();

        AccountDto fakeAccountRequest = createFakeAccountRequest(user.getId(), BigDecimal.ZERO);
        Response response = performPost(fakeAccountRequest);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        AccountResponse initialAccount = response.readEntity(AccountResponse.class);

        response = performDelete(initialAccount.getId());
        assertThat(response.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());

        assertThat(performGetById(initialAccount.getId()).getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void testDeleteNonExistingAccountIsNoop() {
        assertThat(performDelete(NON_EXISTING_ACCOUNT_ID).getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());
    }

    private Response performGetByUserId(String userId) {
        return client.target(toUri(userIdEndpointPath(userId))).request().get();
    }

    @SneakyThrows(UnsupportedEncodingException.class)
    private String userIdEndpointPath(final String userId) {
        return getPath() + "?userId=" + URLEncoder.encode(userId, "UTF-8");
    }

    private AccountDto createInvalidAccountRequest() {
        return new AccountDto();
    }

    private void assertValidResponse(AccountResponse accountResponse, AccountDto fakeAccountRequest) {
        assertThat(accountResponse.getId()).isNotEmpty();
        assertThat(accountResponse.getBalance()).isEqualTo(fakeAccountRequest.getBalance());
        assertThat(accountResponse.getState()).isEqualTo(fakeAccountRequest.getState());
        assertThat(accountResponse.getUserId()).isEqualTo(fakeAccountRequest.getUserId());
    }

    @Override
    protected String getPath() {
        return ACCOUNTS_PATH;
    }
}