package com.example.tmilovanova.moneytransfer.resource;

import com.example.tmilovanova.moneytransfer.exceptions.RestError;
import com.example.tmilovanova.moneytransfer.model.dto.TransferDto;
import com.example.tmilovanova.moneytransfer.model.response.AccountResponse;
import com.example.tmilovanova.moneytransfer.model.response.TransferResponse;
import com.example.tmilovanova.moneytransfer.model.response.UserResponse;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferResourceIT extends AbstractResourceIT {

    private final static String TRANSFERS = "/transfers";

    @Override
    protected String getPath() {
        return TRANSFERS;
    }

    @Test
    public void testGetTransfersByAccountReturnsExistingTransfers() {
        UserResponse user = createUser();
        AccountResponse accountFrom = createAccount(user.getId(), BigDecimal.TEN);
        AccountResponse accountTo = createAccount(user.getId(), BigDecimal.ZERO);

        TransferDto fakeTransferDto = createTransferDto(accountFrom.getId(), accountTo.getId(), BigDecimal.ONE);
        Response transferResponse = performTransfer(fakeTransferDto);

        assertThat(transferResponse).isNotNull();
        assertThat(transferResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        for (String id : Arrays.asList(accountFrom.getId(), accountTo.getId())) {
            Response response = performGetTransfers(id);

            assertThat(response).isNotNull();
            assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
            List<TransferResponse> transferResponses = response.readEntity(new GenericType<List<TransferResponse>>() {
            });
            assertThat(transferResponses).hasSize(1);
            assertValidResponse(transferResponses.get(0), fakeTransferDto);
        }
    }

    @Test
    public void testGetTransfersForNonExistingAccountReturnsEmptyList() {
        Response response = performGetTransfers(NON_EXISTING_ACCOUNT_ID);

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.readEntity(new GenericType<List<AccountResponse>>() {
        })).isEmpty();
    }

    @Test
    public void testTransferMoneyWillSuccessfullyAndCorrectlyChangeAccountStates() {
        UserResponse user = createUser();

        AccountResponse firstAccount = createAccount(user.getId(), BigDecimal.TEN);
        AccountResponse secondAccount = createAccount(user.getId(), BigDecimal.ZERO);

        TransferDto transferDto = createTransferDto(firstAccount.getId(), secondAccount.getId(),
                BigDecimal.ONE);
        Response transferResponse = performTransfer(transferDto);

        assertThat(transferResponse).isNotNull();
        assertThat(transferResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        assertValidResponse(transferResponse.readEntity(TransferResponse.class), transferDto);

        assertThat(performGetById(ACCOUNTS_PATH, firstAccount.getId()).readEntity(AccountResponse.class)
                .getBalance()).isEqualTo(BigDecimal.valueOf(9));
        assertThat(performGetById(ACCOUNTS_PATH, secondAccount.getId()).readEntity(AccountResponse.class)
                .getBalance()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    public void testTransferMoneyWillFailIfFromAccountDoesNotExist() {
        UserResponse user = createUser();
        AccountResponse firstAccount = createAccount(user.getId(), BigDecimal.ONE);

        Response response = performTransfer(createTransferDto(firstAccount.getId(), NON_EXISTING_ACCOUNT_ID, BigDecimal.ONE));

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.ACCOUNT_NOT_FOUND.getMessage());
    }

    @Test
    public void testTransferMoneyWillFailIfFirstAccountHasInsufficientFunds() {
        UserResponse user = createUser();
        AccountResponse firstAccount = createAccount(user.getId(), BigDecimal.ONE);
        AccountResponse secondAccount = createAccount(user.getId(), BigDecimal.ZERO);

        Response response = performTransfer(createTransferDto(firstAccount.getId(), secondAccount.getId(), BigDecimal.TEN));

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.INSUFFICIENT_FUNDS.getMessage());
    }

    @Test
    public void testTransferMoneyWillFailIfToAccountDoesNotExist() {
        UserResponse user = createUser();
        AccountResponse secondAccount = createAccount(user.getId(), BigDecimal.ONE);

        Response response = performTransfer(createTransferDto(NON_EXISTING_ACCOUNT_ID, secondAccount.getId(), BigDecimal.ONE));

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo(RestError.ACCOUNT_NOT_FOUND.getMessage());
    }

    private void assertValidResponse(TransferResponse response, TransferDto fakeTransferDto) {
        assertThat(response.getId()).isNotEmpty();
        assertThat(response.getDate()).isNotNull();
        assertThat(response.getAmount()).isEqualTo(fakeTransferDto.getAmount());
        assertThat(response.getFromAccountId()).isEqualTo(fakeTransferDto.getFromAccountId());
        assertThat(response.getToAccountId()).isEqualTo(fakeTransferDto.getToAccountId());
        assertThat(response.getDescription()).isEqualTo(fakeTransferDto.getDescription());
    }

    private TransferDto createTransferDto(String accountFromId, String accountToId, BigDecimal amount) {
        TransferDto transferRequest = new TransferDto();
        transferRequest.setFromAccountId(accountFromId);
        transferRequest.setToAccountId(accountToId);
        transferRequest.setAmount(amount);
        return transferRequest;
    }

    private Response performTransfer(TransferDto transferRequest) {
        return client.target(toUri(getPath())).request().post(Entity.json(transferRequest));
    }

    private Response performGetTransfers(String accountId) {
        return client.target(toUri(idEndpointPath(accountId))).request().get();
    }
}