package com.example.tmilovanova.moneytransfer.resource;

import com.example.tmilovanova.moneytransfer.MoneyTransferPrototypeApplication;
import com.example.tmilovanova.moneytransfer.configuration.MoneyTransferPrototypeConfiguration;
import com.example.tmilovanova.moneytransfer.model.AccountState;
import com.example.tmilovanova.moneytransfer.model.dto.AccountDto;
import com.example.tmilovanova.moneytransfer.model.dto.UserDto;
import com.example.tmilovanova.moneytransfer.model.response.AccountResponse;
import com.example.tmilovanova.moneytransfer.model.response.UserResponse;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.junit.DropwizardAppRule;
import lombok.SneakyThrows;
import org.junit.BeforeClass;
import org.junit.ClassRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractResourceIT {

    protected static final String NON_EXISTING_USER_ID = "non_existing_user_id";
    protected static final String NON_EXISTING_ACCOUNT_ID = "non_existing_account_id";
    protected static final String USERS_PATH = "/users";
    protected static final String ACCOUNTS_PATH = "/accounts";

    /* Ideally, there should be a test application with test bindings and a test configuration,
     but for now using the main instance for simplicity. */
    @ClassRule
    public static final DropwizardAppRule<MoneyTransferPrototypeConfiguration> RULE =
            new DropwizardAppRule<>(MoneyTransferPrototypeApplication.class, "money-transfer.yml");

    protected static Client client;

    private static final AtomicInteger counter = new AtomicInteger(0);

    @BeforeClass
    public static void setUpClass() {
        client = new JerseyClientBuilder(RULE.getEnvironment()).build("Integration Test Client");
    }

    protected UserResponse createUser() {
        return client.target(toUri(USERS_PATH)).request()
                .post(Entity.json(createFakeUserRequest()))
                .readEntity(UserResponse.class);
    }

    protected AccountResponse createAccount(String userId, BigDecimal amount) {
        return client.target(toUri(ACCOUNTS_PATH)).request()
                .post(Entity.json(createFakeAccountRequest(userId, amount)))
                .readEntity(AccountResponse.class);
    }

    protected UserDto createFakeUserRequest() {
        UserDto request = new UserDto();
        request.setName("John Doe");
        request.setEmail("john" + counter.incrementAndGet() + "@john.doe.org");
        request.setPassword("password");
        return request;
    }

    protected AccountDto createFakeAccountRequest(String userId, BigDecimal amount) {
        AccountDto request = new AccountDto();
        request.setUserId(userId);
        request.setState(AccountState.ACTIVE);
        request.setBalance(amount);
        return request;
    }

    protected Response performPost(Object request) {
        return client.target(toUri(getPath())).request().post(Entity.json(request));
    }

    protected Response performPut(String id, Object request) {
        return client.target(toUri(idEndpointPath(id))).request().put(Entity.json(request));
    }

    protected Response performGetById(String id) {
        return client.target(toUri(idEndpointPath(id))).request().get();
    }

    protected Response performGetById(String path, String id) {
        return client.target(toUri(idEndpointPath(path, id))).request().get();
    }

    protected Object performGetByIdWithObject(String id, Class<?> objectType) {
        return client.target(toUri(idEndpointPath(id))).request().get(objectType);
    }

    protected Response performDelete(String id) {
        return client.target(toUri(idEndpointPath(id))).request().delete();
    }

    protected abstract String getPath();

    @SneakyThrows(UnsupportedEncodingException.class)
    protected String idEndpointPath(String id) {
        return getPath() + "/" + URLEncoder.encode(id, "UTF-8");
    }

    @SneakyThrows(UnsupportedEncodingException.class)
    private String idEndpointPath(String path, String id) {
        return path + "/" + URLEncoder.encode(id, "UTF-8");
    }

    protected String toUri(final String path) {
        return String.format("http://localhost:%d%s", RULE.getLocalPort(), path);
    }
}
