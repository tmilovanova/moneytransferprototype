package com.example.tmilovanova.moneytransfer.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TransactionManagerTest {

    private TransactionManager testInstance;

    private Transactionable first;
    private Transactionable second;

    @Before
    public void init() {
        first = mock(Transactionable.class);
        second = mock(Transactionable.class);
        testInstance = new TransactionManager(Arrays.asList(first, second));
    }

    @Test
    public void testBeginTransactionNotifiesTransactionables() {
        testInstance.beginTransaction();

        verify(first).beginTransaction();
        verify(second).beginTransaction();
    }

    @Test(expected = IllegalStateException.class)
    public void testCannotStartTransactionTwice() {
        testInstance.beginTransaction();
        testInstance.beginTransaction();
    }

    @Test
    public void testCommitNotifiesTransactionables() {
        testInstance.beginTransaction();
        testInstance.commit();

        verify(first).commit();
        verify(second).commit();
    }

    @Test(expected = IllegalStateException.class)
    public void testCannotCommitIfTransactionNotStarted() {
        testInstance.commit();
    }

    @Test
    public void testRollbackNotifiesTransactionables() {
        testInstance.beginTransaction();
        testInstance.rollback();

        verify(first).rollback();
        verify(second).rollback();
    }

    @Test(expected = IllegalStateException.class)
    public void testCannotRollbackIfTransactionNotStarted() {
        testInstance.rollback();
    }
}