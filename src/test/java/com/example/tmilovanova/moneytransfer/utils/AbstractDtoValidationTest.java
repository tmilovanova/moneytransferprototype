package com.example.tmilovanova.moneytransfer.utils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public abstract class AbstractDtoValidationTest<T> {

    protected static final String NOT_BLANK_VALIDATOR_MESSAGE_DESCRIPTOR =
            "org.hibernate.validator.constraints.NotBlank.message";
    protected static final String EMAIL_VALIDATOR_MESSAGE_DESCRIPTOR =
            "org.hibernate.validator.constraints.Email.message";

    private String[] NOT_BLANK_CASES = {null, "", "    "};

    private Validator validator;

    protected AbstractDtoValidationTest() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.validator = vf.getValidator();
    }

    protected void testValidation(T dto, List<String> expectedMessageTemplates) {
        Set<ConstraintViolation<T>> actual = validator.validate(dto);
        List<String> actualSorted = sortViolationsAndExtractTemplates(actual);
        assertThat(actualSorted).isEqualTo(expectedMessageTemplates);
    }

    protected void testNotBlank(T dto, Consumer<String> valueSetter, List<String> expectedMessageTemplates) {
        for (String testCase: NOT_BLANK_CASES) {
            valueSetter.accept(testCase);
            testValidation(dto, expectedMessageTemplates);
        }
    }

    private List<String> sortViolationsAndExtractTemplates(final Collection<ConstraintViolation<T>> actual) {
        return actual
                .stream()
                .sorted((x, y) -> x.getMessageTemplate().compareTo(y.getMessageTemplate()))
                .map(x -> x.getPropertyPath().toString() + "#" + x.getMessageTemplate())
                .collect(Collectors.toList());
    }

    protected abstract T createValidDto();
}
