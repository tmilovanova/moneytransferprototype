package com.example.tmilovanova.moneytransfer.validators;

import org.junit.Test;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class DateFormatValidatorTest {

    private static final String FAKE_ERROR_STRING = "FAKE_VALIDATOR_ERROR";

    private ConstraintValidatorContext mockContext;
    private ConstraintViolationBuilder mockConstraintViolationBuilder;

    @Test
    public void testDateValidatorReturnsTrueForValidAndEmptyFormats() {

        DateFormatValidator testInstance = initializeDateFormatValidator(true);
        resetStateMocks();

        final String[] testCases = {
                "yyyy-MM-dd",
                "yyyy-MM-dd HH:mm:ss.SSS",
                "",
                null,
                "    "
        };
        test(testInstance, testCases, true);
    }

    @Test
    public void testDateValidatorThatNotAllowsEmptyAndBlankValuesFailsOnThem() {
        DateFormatValidator testInstance = initializeDateFormatValidator(false);
        resetStateMocks();

        final String[] testCases = { null, "", "   " };
        test(testInstance, testCases, false);
    }

    @Test
    public void testDateValidatorReturnsFalseForInvalidFormats() {
        DateFormatValidator testInstance = initializeDateFormatValidator(true);
        resetStateMocks();

        final String[] testCases = {
                "invalid_format",
                "xxxx-YY-zz"
        };
        test(testInstance, testCases, false);
    }

    private void test(DateFormatValidator testInstance, String[] testCases, boolean expected) {
        for (String testCase : testCases) {
            boolean result = testInstance.isValid(testCase, mockContext);
            assertThat(result).isEqualTo(expected);

            if (!result) {
                verify(mockContext).disableDefaultConstraintViolation();
                verify(mockConstraintViolationBuilder).addConstraintViolation();
            } else {
                verifyZeroInteractions(mockContext);
                verifyZeroInteractions(mockConstraintViolationBuilder);
            }
            resetStateMocks();
        }
    }

    private DateFormatValidator initializeDateFormatValidator(boolean allowNullAndEmpty) {
        DateFormatValidator dateFormatValidator = new DateFormatValidator();
        ValidDateFormat fakeConstraintAnnotation = mock(ValidDateFormat.class);
        when(fakeConstraintAnnotation.message()).thenReturn(FAKE_ERROR_STRING);
        when(fakeConstraintAnnotation.allowNullAndBlankValues()).thenReturn(allowNullAndEmpty);
        dateFormatValidator.initialize(fakeConstraintAnnotation);
        return dateFormatValidator;
    }

    private void resetStateMocks() {
        mockContext = mock(ConstraintValidatorContext.class);
        mockConstraintViolationBuilder = mock(ConstraintViolationBuilder.class);
        when(mockContext.buildConstraintViolationWithTemplate(eq(FAKE_ERROR_STRING)))
                .thenReturn(mockConstraintViolationBuilder);
    }
}