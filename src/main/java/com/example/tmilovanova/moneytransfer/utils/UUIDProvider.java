package com.example.tmilovanova.moneytransfer.utils;

public interface UUIDProvider {

    String get();
}
