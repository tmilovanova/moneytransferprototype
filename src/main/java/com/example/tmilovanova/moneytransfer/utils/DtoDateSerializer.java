package com.example.tmilovanova.moneytransfer.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.tmilovanova.moneytransfer.dependencies.MoneyTransferPrototypeDependencyModule.EXTERNAL_TIMESTAMP_FORMAT;

/**
 * Seerializer for the external {@code Date} fields in the request mappings.
 * The date format pattern can be specified via the project YML {@code externalTimestampFormat}
 * property, or, if it's not set, the default format {@code yyyy-MM-dd HH:mm:ss} will be used.
 */
public class DtoDateSerializer extends JsonSerializer<Date> {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String dateFormat;

    @Inject
    public DtoDateSerializer(@Named(EXTERNAL_TIMESTAMP_FORMAT) String dateFormat) {
        if (dateFormat == null || dateFormat.trim().isEmpty()) {
            this.dateFormat = DEFAULT_DATE_FORMAT;
        } else {
            this.dateFormat = dateFormat;
        }
    }

    @Override
    public void serialize(final Date value, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
        gen.writeString(new SimpleDateFormat(dateFormat).format(value));
    }
}
