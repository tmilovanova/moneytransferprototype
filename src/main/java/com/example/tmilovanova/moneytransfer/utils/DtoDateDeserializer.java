package com.example.tmilovanova.moneytransfer.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.tmilovanova.moneytransfer.dependencies.MoneyTransferPrototypeDependencyModule.EXTERNAL_TIMESTAMP_FORMAT;

/**
 * Deserializer for the external {@code Date} fields in the request mappings.
 * The date format pattern can be specified via the project YML {@code externalTimestampFormat}
 * property, or, if it's not set, the default format {@code yyyy-MM-dd HH:mm:ss} will be used.
 */
public class DtoDateDeserializer extends JsonDeserializer<Date> {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String dateFormat;

    @Inject
    public DtoDateDeserializer(@Named(EXTERNAL_TIMESTAMP_FORMAT) String dateFormat) {
        if (dateFormat == null || dateFormat.trim().isEmpty()) {
            this.dateFormat = DEFAULT_DATE_FORMAT;
        } else {
            this.dateFormat = dateFormat;
        }
    }

    @Override
    public Date deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            simpleDateFormat.setLenient(false);
            return simpleDateFormat.parse(p.getText());
        } catch (Exception e) {
            throw new JsonProcessingException(e) {
            };
        }
    }
}
