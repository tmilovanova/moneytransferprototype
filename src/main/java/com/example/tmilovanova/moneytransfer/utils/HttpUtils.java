package com.example.tmilovanova.moneytransfer.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpUtils {

    /* Javax-RS's Response.Status does not have this one */
    public static int UNPROCESSABLE_ENTITY_CODE = 422;
}
