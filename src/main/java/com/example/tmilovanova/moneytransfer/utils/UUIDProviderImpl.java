package com.example.tmilovanova.moneytransfer.utils;

import java.util.UUID;

public class UUIDProviderImpl implements UUIDProvider {

    @Override
    public String get() {
        return UUID.randomUUID().toString();
    }
}
