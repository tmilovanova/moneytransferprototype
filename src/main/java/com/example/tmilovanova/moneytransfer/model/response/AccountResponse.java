package com.example.tmilovanova.moneytransfer.model.response;

import com.example.tmilovanova.moneytransfer.model.Account;
import com.example.tmilovanova.moneytransfer.model.dto.AccountDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AccountResponse extends AccountDto {

    private String id;

    public static AccountResponse fromAccount(Account account) {
        AccountResponse response = new AccountResponse();
        response.setId(account.getId());
        response.setBalance(account.getBalance());
        response.setState(account.getState());
        response.setUserId(account.getUserId());
        return response;
    }
}
