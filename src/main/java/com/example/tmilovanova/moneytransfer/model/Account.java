package com.example.tmilovanova.moneytransfer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * Model class representing the Account entity.
 * A User transfers money from an Account that belongs to him/her to another Account.
 * This class is immutable.
 */
@Getter
@AllArgsConstructor
public class Account {

    private String id;
    private String userId;
    private BigDecimal balance;
    private AccountState state;

    /**
     * Creates an instance of {@link Account} with ID field set and other fields copied.
     */
    public Account withId(final String newId) {
        return new Account(newId, userId, balance, state);
    }

    /**
     * Withdraws a specific amount from the account. This method does not check if the withdraw operation
     * is possible.
     *
     * @param amount the amount of money to withdraw
     * @return an account with updated amount of money
     */
    public Account withdraw(final BigDecimal amount) {
        return new Account(id, userId, balance.subtract(amount), state);
    }

    /**
     * Deposits a specific amount of money to the account.
     *
     * @param amount the amount of money to deposit
     * @return an account with updated amount of money
     */
    public Account deposit(final BigDecimal amount) {
        return new Account(id, userId, balance.add(amount), state);
    }
}
