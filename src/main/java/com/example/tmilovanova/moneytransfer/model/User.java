package com.example.tmilovanova.moneytransfer.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Model class representing the User entity.
 * User is the owner of an Account (or Accounts).
 * This class is immutable.
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class User {

    private String id;
    private String name;
    private String password;
    private String email;

    public User withId(final String newId) {
        return new User(newId, name, password, email);
    }
}
