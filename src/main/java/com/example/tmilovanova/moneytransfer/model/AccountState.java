package com.example.tmilovanova.moneytransfer.model;

public enum AccountState {
    ACTIVE, BLOCKED
}
