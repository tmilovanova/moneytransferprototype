package com.example.tmilovanova.moneytransfer.model.dto;

import com.example.tmilovanova.moneytransfer.model.Transfer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferDto {

    @NotBlank
    private String fromAccountId;

    @NotBlank
    private String toAccountId;

    @NotNull
    private BigDecimal amount;

    private String description;

    public Transfer toTransfer() {
        return new Transfer("", new Date(), fromAccountId, toAccountId, amount, description);
    }
}
