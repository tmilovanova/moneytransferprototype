package com.example.tmilovanova.moneytransfer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Model class for representing the Transfer entity.
 * A Transfer is a "history" record about an operation of transferring some amount
 * of money from one Account to another.
 * This class is immutable.
 */
@Getter
@AllArgsConstructor
public class Transfer {
    private String id;
    private Date date;
    private String fromAccountId;
    private String toAccountId;
    private BigDecimal amount;
    private String description;

    public Transfer withId(final String newId) {
        return new Transfer(newId, date, fromAccountId, toAccountId, amount, description);
    }
}
