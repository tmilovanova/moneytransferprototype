package com.example.tmilovanova.moneytransfer.model.dto;

import com.example.tmilovanova.moneytransfer.model.Account;
import com.example.tmilovanova.moneytransfer.model.AccountState;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDto {

    /**
     * My opinion is that the DTO object and the model should be obligatory separated.
     * However, when a model has a lot of properties, often DTO has a big part of them,
     * and that raises the question of how conveniently convert a DTO to a model object
     * and vice versa.
     * For that, we can use Apache Common's BeanUtils.copyProperties(...) or even a library
     * (like Dozer). However, these approaches are based on Java Reflection API, so if performance
     * is key and these operations are repeated often, some code generating solution should be considered.
     * <p>
     * For this case, I have created mapping methods only as the most simple solution.
     */

    @NotBlank
    private String userId;

    @NotNull
    private BigDecimal balance;

    @NotNull
    private AccountState state;

    public Account toAccount() {
        return new Account("", userId, balance, state);
    }
}
