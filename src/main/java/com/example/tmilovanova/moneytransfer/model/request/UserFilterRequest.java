package com.example.tmilovanova.moneytransfer.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.ws.rs.QueryParam;

/**
 * The request mapping for the Find User request.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserFilterRequest {

    @NotBlank
    @Email
    @QueryParam("email")
    private String email;
}
