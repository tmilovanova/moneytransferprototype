package com.example.tmilovanova.moneytransfer.model.response;

import com.example.tmilovanova.moneytransfer.model.User;
import com.example.tmilovanova.moneytransfer.model.dto.UserDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class UserResponse extends UserDto {

    private String id;

    public static UserResponse fromUser(User user) {
        UserResponse response = new UserResponse();
        response.setId(user.getId());
        response.setName(user.getName());
        response.setPassword(user.getPassword());
        response.setEmail(user.getEmail());
        return response;
    }
}
