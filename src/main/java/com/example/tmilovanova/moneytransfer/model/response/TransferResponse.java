package com.example.tmilovanova.moneytransfer.model.response;

import com.example.tmilovanova.moneytransfer.model.Transfer;
import com.example.tmilovanova.moneytransfer.model.dto.TransferDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TransferResponse extends TransferDto {

    private String id;

    private Date date;

    public static TransferResponse fromTransfer(Transfer transfer) {
        TransferResponse response = new TransferResponse();
        response.setId(transfer.getId());
        response.setAmount(transfer.getAmount());
        response.setDate(transfer.getDate());
        response.setDescription(transfer.getDescription());
        response.setFromAccountId(transfer.getFromAccountId());
        response.setToAccountId(transfer.getToAccountId());
        return response;
    }
}
