package com.example.tmilovanova.moneytransfer.configuration;

import com.example.tmilovanova.moneytransfer.validators.ValidDateFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This is a class holding all the properties from the input YML after the application
 * is initialized. From here it can be distributed to other classes (for example, via Guice injection).
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MoneyTransferPrototypeConfiguration extends Configuration {

    @ValidDateFormat(allowNullAndBlankValues = true)
    @JsonProperty
    private String externalTimestampFormat;
}
