package com.example.tmilovanova.moneytransfer.resource;

import com.codahale.metrics.annotation.Timed;
import com.example.tmilovanova.moneytransfer.exceptions.account.AccountNotFoundException;
import com.example.tmilovanova.moneytransfer.exceptions.user.UserNotFoundException;
import com.example.tmilovanova.moneytransfer.model.Account;
import com.example.tmilovanova.moneytransfer.model.dto.AccountDto;
import com.example.tmilovanova.moneytransfer.model.request.AccountFilterRequest;
import com.example.tmilovanova.moneytransfer.model.response.AccountResponse;
import com.example.tmilovanova.moneytransfer.repository.AccountRepository;
import com.example.tmilovanova.moneytransfer.repository.Transactional;
import com.example.tmilovanova.moneytransfer.repository.UserRepository;
import com.google.inject.Inject;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Inject
    public AccountResource(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @POST
    @Timed(name = "createAccount")
    @Transactional
    public Response createAccount(@Valid @NotNull AccountDto accountRequest, @Context UriInfo uriInfo) {
        ensureUserExists(accountRequest);

        Account newAccount = accountRepository.create(accountRequest.toAccount());
        URI uri = uriInfo.getAbsolutePathBuilder().path(newAccount.getId()).build();
        return Response.created(uri).entity(AccountResponse.fromAccount(newAccount)).build();
    }

    @PUT
    @Path("/{accountId}")
    @Timed(name = "updateAccount")
    @Transactional
    public AccountResponse updateAccount(@NotBlank @PathParam("accountId") String accountId, @Valid @NotNull AccountDto account) {
        return AccountResponse.fromAccount(accountRepository.update(account.toAccount().withId(accountId)));
    }

    @GET
    @Path("/{accountId}")
    @Timed(name = "getAccountById")
    @Transactional
    public AccountResponse getAccountById(@NotBlank @PathParam("accountId") String accountId) {
        return AccountResponse.fromAccount(accountRepository.findById(accountId).orElseThrow(AccountNotFoundException::new));
    }

    @GET
    @Timed(name = "getAccountByUserId")
    @Transactional
    public List<AccountResponse> getAccountsByUserId(@BeanParam @Valid AccountFilterRequest accountFilterRequest) {
        return accountRepository.findByUserId(accountFilterRequest.getUserId()).stream().map(AccountResponse::fromAccount).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{accountId}")
    @Timed(name = "deleteAccount")
    @Transactional
    public Response deleteAccount(@NotBlank @PathParam("accountId") String accountId) {
        accountRepository.delete(accountId);
        return Response.noContent().build();
    }

    private void ensureUserExists(final @Valid @NotNull AccountDto accountRequest) {
        userRepository.findById(accountRequest.getUserId()).orElseThrow(UserNotFoundException::new);
    }
}
