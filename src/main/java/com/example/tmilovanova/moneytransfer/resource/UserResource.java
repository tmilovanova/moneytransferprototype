package com.example.tmilovanova.moneytransfer.resource;

import com.codahale.metrics.annotation.Timed;
import com.example.tmilovanova.moneytransfer.exceptions.user.UserNotFoundException;
import com.example.tmilovanova.moneytransfer.model.User;
import com.example.tmilovanova.moneytransfer.model.dto.UserDto;
import com.example.tmilovanova.moneytransfer.model.request.UserFilterRequest;
import com.example.tmilovanova.moneytransfer.model.response.UserResponse;
import com.example.tmilovanova.moneytransfer.repository.AccountRepository;
import com.example.tmilovanova.moneytransfer.repository.Transactional;
import com.example.tmilovanova.moneytransfer.repository.UserRepository;
import com.google.inject.Inject;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;

    @Inject
    public UserResource(UserRepository repository, AccountRepository accountRepository) {
        this.userRepository = repository;
        this.accountRepository = accountRepository;
    }

    @POST
    @Timed(name = "createUser")
    @Transactional
    public Response createUser(@NotNull @Valid UserDto request, @Context UriInfo uriInfo) {
        User newUser = userRepository.create(request.toUser());
        URI uri = uriInfo.getAbsolutePathBuilder().path(newUser.getId()).build();
        return Response.created(uri).entity(UserResponse.fromUser(newUser)).build();
    }

    @PUT
    @Path("/{id}")
    @Timed(name = "updateUser")
    @Transactional
    public UserResponse updateUser(@NotBlank @PathParam("id") String id, @NotNull @Valid UserDto request) {
        return UserResponse.fromUser(userRepository.update(request.toUser().withId(id)));
    }

    @GET
    @Path("/{id}")
    @Timed(name = "getUserById")
    @Transactional
    public UserResponse getUserById(@NotBlank @PathParam("id") String id) {
        return userRepository.findById(id)
                .map(UserResponse::fromUser)
                .orElseThrow(UserNotFoundException::new);
    }

    @GET
    @Timed(name = "findUser")
    @Transactional
    public UserResponse findUser(@BeanParam @Valid UserFilterRequest userFilter) {
        if (userFilter.getEmail() != null) {
            return userRepository.findByEmail(userFilter.getEmail())
                    .map(UserResponse::fromUser)
                    .orElseThrow(UserNotFoundException::new);
        }
        throw new UserNotFoundException();
    }

    @DELETE
    @Path("/{id}")
    @Timed(name = "deleteUser")
    @Transactional
    public Response deleteUser(@NotBlank @PathParam("id") String id) {
        userRepository.delete(id);
        accountRepository.findByUserId(id).stream().forEach(x -> accountRepository.delete(x.getId()));
        return Response.noContent().build();
    }

}
