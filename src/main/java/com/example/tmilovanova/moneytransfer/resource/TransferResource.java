package com.example.tmilovanova.moneytransfer.resource;

import com.codahale.metrics.annotation.Timed;
import com.example.tmilovanova.moneytransfer.exceptions.account.AccountNotFoundException;
import com.example.tmilovanova.moneytransfer.exceptions.account.InsufficientFundsException;
import com.example.tmilovanova.moneytransfer.model.Account;
import com.example.tmilovanova.moneytransfer.model.Transfer;
import com.example.tmilovanova.moneytransfer.model.dto.TransferDto;
import com.example.tmilovanova.moneytransfer.model.response.TransferResponse;
import com.example.tmilovanova.moneytransfer.repository.AccountRepository;
import com.example.tmilovanova.moneytransfer.repository.Transactional;
import com.example.tmilovanova.moneytransfer.repository.TransferRepository;
import com.google.inject.Inject;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/transfers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;

    @Inject
    public TransferResource(AccountRepository accountRepository, TransferRepository transferRepository) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
    }

    @GET
    @Path("/{accountId}")
    @Timed(name = "getTransfersByAccount")
    @Transactional
    public List<TransferResponse> getTransfersByAccount(@NotBlank @PathParam("accountId") String accountId) {
        return transferRepository.findByAccountId(accountId).stream().map(TransferResponse::fromTransfer).collect(Collectors.toList());
    }

    @POST
    @Timed(name = "transferMoney")
    @Transactional
    public Response transferMoney(@NotNull @Valid TransferDto transferRequest, @Context UriInfo uriInfo) {
        Account fromAccount = accountRepository.findById(transferRequest.getFromAccountId()).orElseThrow(AccountNotFoundException::new);
        Account toAccount = accountRepository.findById(transferRequest.getToAccountId()).orElseThrow(AccountNotFoundException::new);
        ensureSufficientFunds(transferRequest, fromAccount);
        accountRepository.update(fromAccount.withdraw(transferRequest.getAmount()));
        accountRepository.update(toAccount.deposit(transferRequest.getAmount()));
        Transfer transfer = transferRequest.toTransfer();
        TransferResponse response = TransferResponse.fromTransfer(transferRepository.create(transfer));
        URI uri = uriInfo.getAbsolutePathBuilder().path(transfer.getId()).build();
        return Response.created(uri).entity(response).build();
    }

    private void ensureSufficientFunds(final @NotNull @Valid TransferDto transferRequest, final Account fromAccount) {
        if (fromAccount.getBalance().compareTo(transferRequest.getAmount()) < 0) {
            throw new InsufficientFundsException();
        }
    }
}
