package com.example.tmilovanova.moneytransfer.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RestError {
    EMAIL_ALREADY_REGISTERED(409, "The email you have specified is already registered."),
    USER_NOT_FOUND(404, "The user you requested is not found."),
    ACCOUNT_NOT_FOUND(404, "The account you requested is not found."),
    INSUFFICIENT_FUNDS(400, "Insufficient funds."),
    CANNOT_CHANGE_ACCOUNT_OWNER(400, "Changing owner for an account is not supported.");

    private int httpCode;
    private String message;
}
