package com.example.tmilovanova.moneytransfer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * An exception mapper for translating inner code {@link MoneyTransferException}
 * exceptions into user-friendly errors.
 */
public class MoneyTransferExceptionMapper implements ExceptionMapper<MoneyTransferException> {

    @Override
    public Response toResponse(final MoneyTransferException exception) {
        return Response.status(exception.getRestError().getHttpCode())
                .entity(exception.getRestError().getMessage())
                .build();
    }
}
