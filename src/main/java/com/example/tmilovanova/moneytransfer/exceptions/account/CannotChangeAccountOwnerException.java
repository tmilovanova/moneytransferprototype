package com.example.tmilovanova.moneytransfer.exceptions.account;

import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferException;
import com.example.tmilovanova.moneytransfer.exceptions.RestError;

public class CannotChangeAccountOwnerException extends MoneyTransferException {

    public CannotChangeAccountOwnerException() {
        super(RestError.CANNOT_CHANGE_ACCOUNT_OWNER);
    }
}
