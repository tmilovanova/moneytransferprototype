package com.example.tmilovanova.moneytransfer.exceptions.account;

import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferException;
import com.example.tmilovanova.moneytransfer.exceptions.RestError;

public class InsufficientFundsException extends MoneyTransferException {

    public InsufficientFundsException() {
        super(RestError.INSUFFICIENT_FUNDS);
    }
}
