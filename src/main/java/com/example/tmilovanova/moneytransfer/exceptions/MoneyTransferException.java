package com.example.tmilovanova.moneytransfer.exceptions;

import lombok.Getter;

/**
 * A generic runtime exception for REST operations.
 */
public abstract class MoneyTransferException extends RuntimeException {

    @Getter
    private final RestError restError;

    public MoneyTransferException(RestError restError) {
        super(restError.getMessage());
        this.restError = restError;
    }

    public MoneyTransferException(RestError restError, Throwable cause) {
        super(restError.getMessage(), cause);
        this.restError = restError;
    }
}
