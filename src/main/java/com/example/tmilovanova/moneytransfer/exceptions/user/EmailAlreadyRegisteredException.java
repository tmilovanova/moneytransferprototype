package com.example.tmilovanova.moneytransfer.exceptions.user;

import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferException;
import com.example.tmilovanova.moneytransfer.exceptions.RestError;

public class EmailAlreadyRegisteredException extends MoneyTransferException {

    public EmailAlreadyRegisteredException() {
        super(RestError.EMAIL_ALREADY_REGISTERED);
    }
}
