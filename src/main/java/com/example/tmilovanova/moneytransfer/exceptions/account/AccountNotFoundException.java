package com.example.tmilovanova.moneytransfer.exceptions.account;

import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferException;
import com.example.tmilovanova.moneytransfer.exceptions.RestError;

public class AccountNotFoundException extends MoneyTransferException {

    public AccountNotFoundException() {
        super(RestError.ACCOUNT_NOT_FOUND);
    }
}
