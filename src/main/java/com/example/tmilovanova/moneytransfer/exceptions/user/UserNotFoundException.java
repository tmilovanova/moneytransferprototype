package com.example.tmilovanova.moneytransfer.exceptions.user;

import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferException;
import com.example.tmilovanova.moneytransfer.exceptions.RestError;

public class UserNotFoundException extends MoneyTransferException {

    public UserNotFoundException() {
        super(RestError.USER_NOT_FOUND);
    }
}
