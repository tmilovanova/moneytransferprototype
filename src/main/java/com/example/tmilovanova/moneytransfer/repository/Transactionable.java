package com.example.tmilovanova.moneytransfer.repository;

/**
 * Interface for representing an entity that supports atomic transactions.
 */
public interface Transactionable {

    void beginTransaction();

    void rollback();

    void commit();
}
