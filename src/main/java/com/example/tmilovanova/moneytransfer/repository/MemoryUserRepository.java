package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.exceptions.user.EmailAlreadyRegisteredException;
import com.example.tmilovanova.moneytransfer.exceptions.user.UserNotFoundException;
import com.example.tmilovanova.moneytransfer.model.User;
import com.example.tmilovanova.moneytransfer.utils.UUIDProvider;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * In-memory implementation for the {@link UserRepository}.
 */
public class MemoryUserRepository implements UserRepository {

    private Map<String, User> users = new HashMap<>();
    private Map<String, User> copy;
    private final UUIDProvider uuidProvider;

    @Inject
    public MemoryUserRepository(UUIDProvider uuidProvider) {
        this.uuidProvider = uuidProvider;
    }

    @Override
    public User create(final User user) throws EmailAlreadyRegisteredException {
        final User withId = user.withId(uuidProvider.get());
        ensureUniqueEmail(withId);
        users.put(withId.getId(), withId);
        return withId;
    }

    @Override
    public User update(final User user) throws UserNotFoundException {
        if (!users.containsKey(user.getId())) {
            throw new UserNotFoundException();
        }
        ensureUniqueEmail(user);
        users.put(user.getId(), user);
        return user;
    }

    @Override
    public Optional<User> findById(final String id) {
        return Optional.ofNullable(users.get(id));
    }

    /* In a real repository that connects to a database we should have a view or an index
     * for the email field. This implementation obviously will not scale well. */
    @Override
    public Optional<User> findByEmail(final String email) {
        return users.values().stream()
                .filter(x -> x.getEmail().equals(email))
                .findFirst();
    }

    @Override
    public void delete(final String id) {
        users.remove(id);
    }

    @Override
    public void beginTransaction() {
        copy = new HashMap<>(users);
    }

    @Override
    public void rollback() {
        users = copy;
        copy = null;
    }

    @Override
    public void commit() {
        copy = null;
    }

    private void ensureUniqueEmail(final User newUserWithEmail) {
        Optional<User> existingUser = findByEmail(newUserWithEmail.getEmail());
        if (existingUser.isPresent() && !newUserWithEmail.getId().equals(existingUser.get().getId())) {
            throw new EmailAlreadyRegisteredException();
        }
    }
}
