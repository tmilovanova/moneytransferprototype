package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.model.Transfer;
import com.example.tmilovanova.moneytransfer.utils.UUIDProvider;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemoryTransferRepository implements TransferRepository, Transactionable {

    private Map<String, List<Transfer>> byAccount = new HashMap<>();
    private Map<String, Transfer> transfers = new HashMap<>();
    private Map<String, List<Transfer>> byAccountCopy;
    private Map<String, Transfer> transfersCopy;

    private final UUIDProvider uuidProvider;

    @Inject
    public MemoryTransferRepository(UUIDProvider uuidProvider) {
        this.uuidProvider = uuidProvider;
    }

    @Override
    public Transfer create(final Transfer transfer) {
        final Transfer withId = transfer.withId(uuidProvider.get());
        transfers.put(withId.getId(), withId);
        addToAccount(withId.getFromAccountId(), withId);
        addToAccount(withId.getToAccountId(), withId);
        return withId;
    }

    private void addToAccount(final String accountId, final Transfer transfer) {
        List<Transfer> accountTransfers = byAccount.get(accountId);
        if (accountTransfers == null) {
            accountTransfers = new ArrayList<>();
            byAccount.put(accountId, accountTransfers);
        }
        accountTransfers.add(transfer);
    }

    @Override
    public List<Transfer> findByAccountId(final String accountId) {
        List<Transfer> result = byAccount.get(accountId);
        if (result == null) {
            return Collections.emptyList();
        }
        return result;
    }

    @Override
    public void delete(final String id) {
        Transfer transfer = transfers.remove(id);
        if (transfer == null) {
            return;
        }
        byAccount.get(transfer.getFromAccountId()).remove(transfer);
        byAccount.get(transfer.getToAccountId()).remove(transfer);
    }

    @Override
    public void beginTransaction() {
        transfersCopy = new HashMap<>(transfers);
        byAccountCopy = new HashMap<>(byAccount);
    }

    @Override
    public void rollback() {
        transfers = transfersCopy;
        transfersCopy = null;
        byAccount = byAccountCopy;
        byAccountCopy = null;
    }

    @Override
    public void commit() {
        transfersCopy = null;
        byAccountCopy = null;
    }
}
