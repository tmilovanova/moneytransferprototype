package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.exceptions.account.AccountNotFoundException;
import com.example.tmilovanova.moneytransfer.exceptions.account.CannotChangeAccountOwnerException;
import com.example.tmilovanova.moneytransfer.model.Account;

import java.util.List;
import java.util.Optional;

/**
 * CRUD interface for working with a database of Account entities.
 * Supports transactional operations by implementing {@link Transactionable}.
 */
public interface AccountRepository extends Transactionable {

    Account create(Account account);

    Optional<Account> findById(String id);

    List<Account> findByUserId(String userId);

    Account update(Account account) throws AccountNotFoundException, CannotChangeAccountOwnerException;

    void delete(String id);
}
