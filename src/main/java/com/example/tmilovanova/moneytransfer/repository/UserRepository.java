package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.exceptions.user.EmailAlreadyRegisteredException;
import com.example.tmilovanova.moneytransfer.exceptions.user.UserNotFoundException;
import com.example.tmilovanova.moneytransfer.model.User;

import java.util.Optional;

/**
 * CRUD interface for working with a database of User entities.
 * Supports transactional operations by implementing {@link Transactionable}.
 */
public interface UserRepository extends Transactionable {

    User create(User user) throws EmailAlreadyRegisteredException;

    User update(User user) throws UserNotFoundException;

    Optional<User> findById(String id);

    Optional<User> findByEmail(String email);

    void delete(String id);

}
