package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.exceptions.account.AccountNotFoundException;
import com.example.tmilovanova.moneytransfer.exceptions.account.CannotChangeAccountOwnerException;
import com.example.tmilovanova.moneytransfer.model.Account;
import com.example.tmilovanova.moneytransfer.utils.UUIDProvider;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class MemoryAccountRepository implements AccountRepository, Transactionable {

    private Map<String, Account> accounts = new HashMap<>();
    private Map<String, Account> accountsCopy;
    private Map<String, List<Account>> byUserId = new HashMap<>();
    private Map<String, List<Account>> byUserIdCopy;

    private final UUIDProvider uuidProvider;

    @Inject
    public MemoryAccountRepository(UUIDProvider uuidProvider) {
        this.uuidProvider = uuidProvider;
    }

    @Override
    public Account create(final Account account) {
        final Account withId = account.withId(uuidProvider.get());
        accounts.put(withId.getId(), withId);
        List<Account> userAccounts = byUserId.get(withId.getUserId());
        if (userAccounts == null) {
            userAccounts = new ArrayList<>();
            byUserId.put(withId.getUserId(), userAccounts);
        }
        userAccounts.add(withId);
        return withId;
    }

    @Override
    public Optional<Account> findById(final String id) {
        return Optional.ofNullable(accounts.get(id));
    }

    @Override
    public List<Account> findByUserId(final String userId) {
        List<Account> results = byUserId.get(userId);
        return results == null ? Collections.emptyList() : new ArrayList<>(results);
    }

    @Override
    public Account update(final Account account) throws AccountNotFoundException, CannotChangeAccountOwnerException {
        Account existingAccount = accounts.get(account.getId());
        if (existingAccount == null) {
            throw new AccountNotFoundException();
        }
        if (!existingAccount.getUserId().equals(account.getUserId())) {
            throw new CannotChangeAccountOwnerException();
        }
        accounts.put(account.getId(), account);
        return account;
    }

    @Override
    public void delete(final String id) {
        Account account = accounts.remove(id);
        if (account == null) {
            return;
        }
        List<Account> accounts = byUserId.get(account.getUserId());
        accounts.remove(account);
    }

    @Override
    public void beginTransaction() {
        accountsCopy = new HashMap<>(accounts);
        byUserIdCopy = new HashMap<>(byUserId);
    }

    @Override
    public void rollback() {
        accounts = accountsCopy;
        accountsCopy = null;
        byUserId = byUserIdCopy;
        byUserIdCopy = null;
    }

    @Override
    public void commit() {
        accountsCopy = null;
        byUserIdCopy = null;
    }
}
