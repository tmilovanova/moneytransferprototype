package com.example.tmilovanova.moneytransfer.repository;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A class managing access to data repositories via transactions so that
 * few access operations to the repository were atomic. Otherwise, if we get
 * parallel requests and are performing GET - POST from one thread, another thread
 * can change the state of the data in such a way that the first GET will not be valid anymore.
 * The class does not allow nested transactions.
 *
 * Note: This implementation only makes the transactions sequential, so no race conditions will
 * happen, but no independent parallel transactions are possible. This is not a very well-performing
 * solution in a real world, but at least it works correctly (hopefully).
 */
public class TransactionManager implements Transactionable {

    private ReentrantLock lock = new ReentrantLock();
    private List<Transactionable> transactionables;

    public TransactionManager(List<Transactionable> transactionables) {
        this.transactionables = transactionables;
    }

    public void beginTransaction() {
        if (lock.isHeldByCurrentThread()) {
            throw new IllegalStateException("Transfer is already in progress");
        }

        lock.lock();
        for (Transactionable transactionable : transactionables) {
            transactionable.beginTransaction();
        }
    }

    public void rollback() {
        if (!lock.isHeldByCurrentThread()) {
            throw new IllegalStateException("Trying to rollback a transaction that is not in progress");
        }

        for (Transactionable transactionable : transactionables) {
            transactionable.rollback();
        }
        lock.unlock();
    }

    public void commit() {
        if (!lock.isHeldByCurrentThread()) {
            throw new IllegalStateException("Trying to commit a transaction that is not in progress");
        }

        for (Transactionable transactionable : transactionables) {
            transactionable.commit();
        }
        lock.unlock();
    }
}
