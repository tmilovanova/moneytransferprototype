package com.example.tmilovanova.moneytransfer.repository;

import com.example.tmilovanova.moneytransfer.model.Transfer;

import java.util.List;

/**
 * CRUD interface for working with a database of Transfer entities.
 * Supports transactional operations by implementing {@link Transactionable}.
 */
public interface TransferRepository extends Transactionable {

    Transfer create(Transfer transfer);

    List<Transfer> findByAccountId(String accountId);

    void delete(String id);
}
