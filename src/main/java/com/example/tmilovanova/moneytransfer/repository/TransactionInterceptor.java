package com.example.tmilovanova.moneytransfer.repository;

import com.google.inject.Inject;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class TransactionInterceptor implements MethodInterceptor {

    @Inject
    private TransactionManager transactionManager;

    @Override
    public Object invoke(final MethodInvocation methodInvocation) throws Throwable {
        transactionManager.beginTransaction();
        try {
            Object result = methodInvocation.proceed();
            transactionManager.commit();
            return result;
        } catch (Throwable e) {
            transactionManager.rollback();
            throw e;
        }
    }
}
