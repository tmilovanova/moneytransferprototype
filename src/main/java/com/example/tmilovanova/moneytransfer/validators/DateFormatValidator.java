package com.example.tmilovanova.moneytransfer.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;

/**
 * Validator for a date format pattern represented by a {@link String}.
 */
public class DateFormatValidator implements ConstraintValidator<ValidDateFormat, String> {

    private String errorMessage;
    private boolean allowNullAndBlankValues;

    @Override
    public void initialize(final ValidDateFormat constraintAnnotation) {
        errorMessage = constraintAnnotation.message();
        allowNullAndBlankValues = constraintAnnotation.allowNullAndBlankValues();
    }

    /**
     * This function determines if the input {@code String} is a valid date format.
     * Null, empty or blank values are allowed or disallowed depending on the value
     * of the {@link ValidDateFormat#allowNullAndBlankValues()} in the constraint annotation.
     *
     * @return true if a value is a valid date format pattern, false otherwise
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        boolean isValid;
        try {
            if (value == null || value.trim().isEmpty()) {
                isValid = allowNullAndBlankValues;
            } else {
                new SimpleDateFormat(value);
                isValid = true;
            }
        } catch (Exception ex) {
            isValid = false;
        }
        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(errorMessage).addConstraintViolation();
        }
        return isValid;
    }
}
