package com.example.tmilovanova.moneytransfer.dependencies;

import com.example.tmilovanova.moneytransfer.configuration.MoneyTransferPrototypeConfiguration;
import com.example.tmilovanova.moneytransfer.repository.AccountRepository;
import com.example.tmilovanova.moneytransfer.repository.MemoryAccountRepository;
import com.example.tmilovanova.moneytransfer.repository.MemoryTransferRepository;
import com.example.tmilovanova.moneytransfer.repository.MemoryUserRepository;
import com.example.tmilovanova.moneytransfer.repository.TransactionInterceptor;
import com.example.tmilovanova.moneytransfer.repository.TransactionManager;
import com.example.tmilovanova.moneytransfer.repository.Transactional;
import com.example.tmilovanova.moneytransfer.repository.TransferRepository;
import com.example.tmilovanova.moneytransfer.repository.UserRepository;
import com.example.tmilovanova.moneytransfer.utils.UUIDProvider;
import com.example.tmilovanova.moneytransfer.utils.UUIDProviderImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Names;

import java.util.Arrays;

public class MoneyTransferPrototypeDependencyModule extends AbstractModule {

    private final MoneyTransferPrototypeConfiguration configuration;

    public static final String EXTERNAL_TIMESTAMP_FORMAT = "externalTimestampFormat";

    public MoneyTransferPrototypeDependencyModule(final MoneyTransferPrototypeConfiguration configuration) {
        this.configuration = configuration;
    }

    protected void configure() {
        bindConstant().annotatedWith(Names.named(EXTERNAL_TIMESTAMP_FORMAT)).to(configuration.getExternalTimestampFormat());

        bind(UserRepository.class).to(MemoryUserRepository.class).asEagerSingleton();
        bind(AccountRepository.class).to(MemoryAccountRepository.class).asEagerSingleton();
        bind(TransferRepository.class).to(MemoryTransferRepository.class).asEagerSingleton();

        bind(UUIDProvider.class).to(UUIDProviderImpl.class).in(Singleton.class);

        TransactionInterceptor transactionInterceptor = new TransactionInterceptor();
        requestInjection(transactionInterceptor);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Transactional.class));
    }

    @Provides
    @Singleton
    public TransactionManager transactionManager(UserRepository userRepository,
            AccountRepository accountRepository, TransferRepository transferRepository) {
        return new TransactionManager(Arrays.asList(userRepository, accountRepository, transferRepository));
    }
}
