package com.example.tmilovanova.moneytransfer;

import com.example.tmilovanova.moneytransfer.configuration.MoneyTransferPrototypeConfiguration;
import com.example.tmilovanova.moneytransfer.dependencies.MoneyTransferPrototypeDependencyModule;
import com.example.tmilovanova.moneytransfer.exceptions.MoneyTransferExceptionMapper;
import com.example.tmilovanova.moneytransfer.resource.AccountResource;
import com.example.tmilovanova.moneytransfer.resource.TransferResource;
import com.example.tmilovanova.moneytransfer.resource.UserResource;
import com.example.tmilovanova.moneytransfer.utils.DtoDateDeserializer;
import com.example.tmilovanova.moneytransfer.utils.DtoDateSerializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.util.Date;

/**
 * This is the main application entry point in a Dropwizard-based application.
 *
 * @see <a href="http://www.dropwizard.io/1.0.6/docs/getting-started.html>Dropwizard Getting Started guide</>
 */
public class MoneyTransferPrototypeApplication extends Application<MoneyTransferPrototypeConfiguration> {

    public static void main(String[] args) throws Exception {
        new MoneyTransferPrototypeApplication().run(args);
    }

    @Override
    public String getName() {
        return "Money Transfer Prototype Application";
    }

    @Override
    public void initialize(Bootstrap<MoneyTransferPrototypeConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(MoneyTransferPrototypeConfiguration configuration, Environment environment) {
        Injector injector = createInjector(configuration);

        /* Registering resources - classes that contain REST endpoint methods */
        registerCustomSerializers(injector, environment);
        registerResources(environment, injector);
        registerExceptionMappers(environment);
    }

    private void registerCustomSerializers(Injector injector, final Environment environment) {
        SimpleModule module = new SimpleModule("DateDto");
        module.addDeserializer(Date.class, injector.getInstance(DtoDateDeserializer.class));
        module.addSerializer(Date.class, injector.getInstance(DtoDateSerializer.class));
        environment.getObjectMapper().registerModule(module);
    }

    private void registerExceptionMappers(final Environment environment) {
        environment.jersey().register(MoneyTransferExceptionMapper.class);
    }

    private void registerResources(final Environment environment, final Injector injector) {
        environment.jersey().register(injector.getInstance(UserResource.class));
        environment.jersey().register(injector.getInstance(AccountResource.class));
        environment.jersey().register(injector.getInstance(TransferResource.class));
    }

    private Injector createInjector(final MoneyTransferPrototypeConfiguration configuration) {
        return Guice.createInjector(new MoneyTransferPrototypeDependencyModule(configuration));
    }
}