# README #

My implementation of the Java / Scala task.

## How do I get set up? ##

This is a Maven-driven project using dropwizard.io framework.

To build the project, run

```
mvn clean install

```
on the command line inside the project root directory.
To run the server, execute
```
java -jar target/MoneyTransferPrototype-1.0-SNAPSHOT.jar server money-transfer.yml
```
where money-transfer.yml is the typical Dropwizard YML configuration file. Currently it only has one setting.
After that, the application will run on http://localhost:8080/ .

##Notes:##
The application uses JAX-RS (Jersey) for REST, JSON only for input and output. It has 3 main paths: /users (in other words, account holders), /accounts and /transfers for managing the respective entities. Code-wise, the REST endpoints declaration and logic are placed in *Resource.java files.
Other libraries used:

* Guice for dependency injection,
* Lombok for boilerplate code generation,
* Jackson for JSON (is already integrated in Dropwizard),
* JUnit4, Mockito, AssertJ for testing.

There are three main integration tests (*IT.java) as well as several unit tests. All of them are taken up by the Maven build. I wouldn't say the test coverage is complete, I see many ways for improvement.

The following rules were (or, at least were intended to be) implemented:

1. Every external entity (User, Account or Transfer) has UUID as its ID. For user, one more important constraint applies: it must have a unique e-mail. Also, a bunch of other not-null and not-empty constraints exist (see the *Dto.java classes).
2. Users can have multiple accounts, one account belongs only to one user. Accounts can have multiple Transfers, every transfer is only linked to 2 accounts - the "from" one and the "two" one.
The name "Transfer" was chosen to avoid ambiguity since there are some classes related to Transaction in the code, but they have quite a different meaning and are destined to ensure operations atomicity and integrity (see TransactionManager.java).
3. An account cannot be created (generally, should not exist) without an existing user. Consequently, on user deletion all his/her accounts are supposed to be deleted.
4. A user cannot do a transfer if his/her funds are insufficient (no overdraft). Also, the currencies are not taken into account, as discussed.

##What's not perfect notes:##
As next (necessary) steps for the application development I would also add:

* Definitely improve test coverage (add serialization / deserialization test cases, separate tests for resources with mocked repositories, validation tests for Account and Transfer and more transaction-related testing);
* Swagger support for clear endpoint documentation and a nice UI;
* logging (for instance, with SL4J);
* Dropwizard healthchecks;
* Work on the suggestions I put in the comments in the code;
* Last, but not least, integration tests can be refactored further for code duplication elimination (mostly in the setup part of the test).